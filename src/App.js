import './App.css';
import React from 'react';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            users: []
        };
    }

    componentDidMount() {
        fetch('http://127.0.0.1:8000/api/users')
            .then((response) => response.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.users
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render() {
        console.log(this.state.items)
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (

                <div className={'container mt-5'}>
                    <h5>Usuarios recojidos de la función getUsers</h5>
                    <ul>
                        {items.map(item => (
                            <li key={item.id} id={item.id}>
                                <b>{item.name}</b><br/>
                                <i>{item.description}</i><br/>
                                <b>foto:</b><a href={item.imageURL}> {item.name}</a>
                                <hr/>
                            </li>
                        ))}
                    </ul>

                </div>
            );
        }
    }
}


export default App;
